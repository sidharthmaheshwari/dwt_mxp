/**
 * VECTORBLOX MXP SOFTWARE DEVELOPMENT KIT
 *
 * Copyright (C) 2012-2014 VectorBlox Computing Inc., Vancouver, British Columbia, Canada.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *     * Neither the name of VectorBlox Computing Inc. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This agreement shall be governed in all respects by the laws of the Province
 * of British Columbia and by the laws of Canada.
 *
 * This file is part of the VectorBlox MXP Software Development Kit.
 *
 */

#include "vbx.h"
#include "vbx_port.h"
#include <stdio.h>
#include <string.h>
#include <inttypes.h>
#include <stdint.h>
 #include <stdlib.h>
#define BUFFER_SIZE 400000
#define MATRIX_SIZE 2048
#define DATA_ARRAY_SIZE 2048

int col1[DATA_ARRAY_SIZE]; 
int TwoD[MATRIX_SIZE][MATRIX_SIZE],TwoD_T[MATRIX_SIZE][MATRIX_SIZE];

void ParseCSV_Data( FILE *pinput, int *a )
{
	int i = 0;
	char buf[BUFFER_SIZE];
	while (fgets(buf, sizeof(buf), pinput) != NULL && i< (DATA_ARRAY_SIZE))
	{
		*a = *a+1; 
		char *tok = strtok(buf, " ,");
		//while (tok != NULL ) //&& j<=MATRIX_SIZE
		//  	{
		  		//printf("%s \n", tok);
				col1[i] = atoi(tok); 
		//		tok = strtok(NULL, " ,");
				 i++;
		//  	}
		
	}
}

void ParseCSV_TransMat(FILE *TM)
{
	int k = 0; int j = 0;
	char buf[BUFFER_SIZE];
	while (fgets(buf, sizeof(buf), TM) != NULL ) //&& k < MATRIX_SIZE
	{
		char *tok = strtok(buf, " ,"); j = 0;
			while (tok != NULL) // && j < MATRIX_SIZE
		  	{
			    TwoD[k][j] = atoi(tok);  j++;
			    tok = strtok(NULL, " ,");
		  	}
	  	k++;	
	}
}
void ParseCSV_TransMat_T(FILE *TM_T)
{
	int k = 0; int j = 0;
	char buf[BUFFER_SIZE];
	while (fgets(buf, sizeof(buf), TM_T) != NULL ) //&& k < MATRIX_SIZE
	{
		char *tok = strtok(buf, " ,"); j = 0;
			while (tok != NULL) // && j < MATRIX_SIZE
		  	{
			    TwoD_T[k][j] = atoi(tok);  j++;
			    tok = strtok(NULL, " ,");
		  	}
	  	k++;	
	}
}

int main(void)
{

	FILE *pinput, *TM, *TM_T;
	int lines = 0;
	pinput = fopen("Patient1_lead1_32768_FXD.txt", "r");
	TM = fopen("TM_FXD.txt","r");
	if (pinput == NULL) {
	  fprintf(stderr, "Can't open input file in.list!\n");
	  exit(1);
	}
	if (TM == NULL) {
	  fprintf(stderr, "Can't open input file in.list!\n");
	  exit(1);
	}
	TM_T = fopen("TM_FXD_T.txt","r");
	if (TM_T == NULL) {
	  fprintf(stderr, "Can't open input file in.list!\n");
	  exit(1);
	}
	
	ParseCSV_Data(pinput,&lines);
	ParseCSV_TransMat(TM);
	ParseCSV_TransMat_T(TM_T);

	int len_col1 = sizeof(col1)/ sizeof(col1[0]);
	//printf("Length of data array - %d\n", len_col1);
	//int a[] = {19520,6502,-827,-2112,0};
	//int b[] = {-32047000,-31785000,-31654000,-31588000,-30343000};
	int output, coeff[MATRIX_SIZE], inp[MATRIX_SIZE];//{0,0,0,0,0};
	int i,z,k;
	vbx_word_t *vA, *vB, *vD;
	vbxsim_init(16,/*vector_lanes*/
	            64,/*KB scratchpad_size*/
	            256,/*max masked waves*/
	            16,/*fractional_bits (word)*/
	            8,/*fractional_bits (half)*/
	            4/*fractional_bits (byte)*/);
	
	/*
	for (int k=0;k<len_col1;k++)
	{
		printf("%d   %d\n", col1[k], inp[k]);
	}
	*/
	vA = (vbx_word_t *)vbx_sp_malloc( sizeof(coeff) );
	vB = (vbx_word_t *)vbx_sp_malloc( sizeof(coeff) );
	vD = (vbx_word_t *)vbx_sp_malloc( sizeof(int) );
	vbx_set_vl( MATRIX_SIZE );
	vbx_dma_to_vector( vB,  col1, sizeof(coeff));
	for(i = 0;i<2048; i++)
	{		
		//for( z = 0;z<MATRIX_SIZE;z++)
		//{
		//	inp[z] = TwoD[i][z];
		//}
		//printf("Inside for loop No- %d ",i);
		//vbx_dma_to_vector( vA,  inp, sizeof(coeff) );	
		vbx_dma_to_vector( vA,  TwoD + i, sizeof(coeff) );	
		vbx_sync();	
		/*
		for (int k=0;k<len_col1;k++)
		{
		printf("%d   %d  %d   %d\n", col1[k],vB[k],inp[k],vA[k]);
		}
		*/
		vbx_acc( VVWS, VMULFXP, vD, vB, vA );
		vbx_dma_to_host( &output, vD, sizeof(int) );
		vbx_sync();
		coeff[i] = output;
		//printf(" - result - %d  \n", coeff[i]);
	}
	//---------------Saving level 9 coefficients to file-------------
	FILE *f = fopen("coeff9_sim.txt", "w");
	if (f == NULL)
	{
	    printf("Error opening file!\n");
	    exit(1);
	}
	for (k=0;k<MATRIX_SIZE;k++)
	{
	fprintf(f, "%d\n", coeff[k]);
	}
	fclose(f);
	//---------------------------------------------------------------
	//----------Making 9th level approximate coefficients zero-------
	for (k=511;k<MATRIX_SIZE;k=k+512)
	{
	coeff[k] = 0;
	}
	//---------------------------------------------------------------
	

	vbx_dma_to_vector( vB,  coeff, sizeof(coeff));
	for(i = 0;i<2048; i++)
	{				
		vbx_dma_to_vector( vA,  TwoD_T + i, sizeof(coeff) );		
		vbx_acc( VVWS, VMULFXP, vD, vB, vA );
		vbx_dma_to_host( &output, vD, sizeof(int) );
		vbx_sync();
		coeff[i] = output;
	}
	//---------------Saving denoised vector to file-------------
	f = fopen("denoised_sim.txt", "w");
	if (f == NULL)
	{
	    printf("Error opening file!\n");
	    exit(1);
	}
	for (k=0;k<MATRIX_SIZE;k++)
	{
	fprintf(f, "%d\n", coeff[k]);
	}
	fclose(f);
	
	//---------------------------------------------------------------
	vbx_sp_free();
	vbxsim_destroy();
	//for (int k=0;k<20;k++)
	//{
	//	printf("The result - %d  \n", coeff[0]);
		//printf("%d   %d\n", d[0], sizeof(d));
	//}
	fclose(pinput);fclose(TM);fclose(TM_T);
	return 0;
}
