
#include "vectorblox_mxp_lin.h"
#include "vbx_types.h"
#include "vbx_port.h"
#include "vbx_test.h"
#include "vbx_common.h"
#include "vbx.h"
#include "vbx_port.h"
#include <stdio.h>
#include <string.h>
#include <inttypes.h>
#include <stdint.h>
#include <stdlib.h>
#define BUFFER_SIZE 400000
#define MATRIX_SIZE 2048
#define DATA_ARRAY_SIZE 2048
#define MXP


long timestamp()
{
	struct timeval current_time;
	gettimeofday(&current_time, NULL);
	return (current_time.tv_sec * 1000000 + current_time.tv_usec);
}

void ParseCSV_Data( FILE *pinput, int *a, int32_t *col1 )
{
	int i = 0;
	char buf[BUFFER_SIZE];
	while (fgets(buf, sizeof(buf), pinput) != NULL) // && i< (DATA_ARRAY_SIZE)
	{
		*a = *a+1; 
		char *tok = strtok(buf, " ,");
		//while (tok != NULL ) //&& j<=MATRIX_SIZE
		//  	{
		  		//printf("%s \n", tok);
				col1[i] = atoi(tok); 
		//		tok = strtok(NULL, " ,");
				 i++;
		//  	}
		
	}
}

void ParseCSV_TransMat(FILE *TM, int32_t **TwoD)
{
	int k = 0; int j = 0;
	char buf[BUFFER_SIZE];
	while (fgets(buf, sizeof(buf), TM) != NULL) // && k < MATRIX_SIZE
	{
		char *tok = strtok(buf, " ,"); j = 0;
			while (tok != NULL ) // && j < MATRIX_SIZE
		  	{
			    TwoD[k][j] = atoi(tok);  j++;
			    tok = strtok(NULL, " ,");
		  	}
	  	k++;	
	}
}
void ParseCSV_TransMat_T(FILE *TM_T, int32_t **TwoD_T)
{
	int k = 0; int j = 0;
	char buf[BUFFER_SIZE];
	while (fgets(buf, sizeof(buf), TM_T) != NULL ) //&& k < MATRIX_SIZE 
	{
		char *tok = strtok(buf, " ,"); j = 0;
			while (tok != NULL ) // && j < MATRIX_SIZE 
		  	{
			    TwoD_T[k][j] = atoi(tok);  j++;
			    tok = strtok(NULL, " ,");
		  	}
	  	k++;	
	}
}

int main(void)
{
	#ifdef MXP
		printf("MXP Enabled\n");
		VectorBlox_MXP_Initialize("mxp0","cma");
	#else
		printf("MXP Disabled, APP is running entirely on ARM\n");
	#endif
	long start_time, stop_time;
	FILE *pinput, *TM, *TM_T, *f;
	int lines = 0;
	pinput = fopen("Patient1_lead1_32768_FXD.txt", "r");
	TM = fopen("TM_FXD.txt","r");
	if (pinput == NULL) {
	  fprintf(stderr, "Can't open input file in.list!\n");
	  exit(1);
	}
	if (TM == NULL) {
	  fprintf(stderr, "Can't open input file in.list!\n");
	  exit(1);
	}
	TM_T = fopen("TM_FXD_T.txt","r");
	if (TM_T == NULL) {
	  fprintf(stderr, "Can't open input file in.list!\n");
	  exit(1);
	}
	int i,k;
	int32_t *col1 = (int32_t *)vbx_shared_malloc( DATA_ARRAY_SIZE * sizeof(int32_t)); 
	int32_t **TwoD   = (int32_t **) vbx_shared_malloc( MATRIX_SIZE * sizeof(int32_t *));
	int32_t **TwoD_T = (int32_t **) vbx_shared_malloc( MATRIX_SIZE * sizeof(int32_t *));
	int32_t *output = (int32_t *)vbx_shared_malloc( sizeof(int32_t));
	int32_t *coeff = (int32_t *)vbx_shared_malloc( MATRIX_SIZE * sizeof(int32_t)); 
	for(i = 0; i < MATRIX_SIZE; i++)
		{
		*(TwoD + i)   = (int32_t *) vbx_shared_malloc( MATRIX_SIZE * sizeof(int32_t));
		*(TwoD_T + i) = (int32_t *) vbx_shared_malloc( MATRIX_SIZE * sizeof(int32_t));
		if(TwoD[i] == NULL || TwoD_T[i] == NULL)
			{
				printf(" Out of memory.. Cannot allocate space for Transformation matrix \n ");
				printf("------------------ Exiting appication-------------- \n");
				return -1;
			}
		}
	if(coeff == NULL || output == NULL || col1 == NULL)
			{
				printf(" Out of memory.. Cannot allocate space for Transformation matrix \n ");
				printf("------------------ Exiting appication-------------- \n");
				return -1;
			}		
	ParseCSV_Data(pinput,&lines,col1);
	ParseCSV_TransMat(TM,TwoD);
	ParseCSV_TransMat_T(TM_T,TwoD_T);
	
	//{0,0,0,0,0};
	
	int32_t *vA, *vB, *vD;
	//int32_t *vA, *vB, *vD;

	if((NULL ==	(vA = (int32_t *)vbx_sp_malloc(MATRIX_SIZE * sizeof(int32_t)))) ||
	(NULL == (vB = (int32_t *)vbx_sp_malloc(MATRIX_SIZE * sizeof(int32_t)))) ||
	(NULL == (vD = (int32_t *)vbx_sp_malloc( sizeof(int32_t)))))
	{
		printf("No scratchpad... exiting\n");
		return -1;
	}
	vbx_set_vl( MATRIX_SIZE );
	//printf("%d %x  %x \n",lines, col1, vB);

	//printf(" Verification 1 \n");
	start_time = timestamp();
	vbx_dma_to_vector( vB,  col1, MATRIX_SIZE * sizeof(int32_t));

	for(i = 0;i<MATRIX_SIZE; i++)
	{		

	//	printf(" Verification 2 \n");
		vbx_dma_to_vector( vA,  TwoD[i], MATRIX_SIZE * sizeof(int32_t) );	
		//vbx_sync();
		/*	
		for (k=2038;k<2048;k++)
		{
		printf("%d   %d  \n", vA[k],TwoD[i][k]);
		}
		*/
		vbx_acc( VVWS, VMULFXP, vD, vB, vA);
	//	printf(" Verification 3 \n");
		vbx_dma_to_host( output, vD, sizeof(int32_t) );
		vbx_sync();
		coeff[i] = *output;
		//printf(" - result - %d  \n", coeff[i]);
	}
	//---------------Saving level 9 coefficients to file-------------
	//printf(" Verification 4 \n");
		//---------------------------------------------------------------
	//----------Making 9th level approximate coefficients zero-------
	for (k=511;k<MATRIX_SIZE;k=k+512)
	{
	coeff[k] = 0;
	}
	//---------------------------------------------------------------
	/*
	f = fopen("coeff9_sim.txt", "w");
	if (f == NULL)
	{
	    printf("Error opening file!\n");
	    exit(1);
	}
	for (k=0;k<MATRIX_SIZE;k++)
	{
	fprintf(f, "%d\n", coeff[k]);
	}
	fclose(f);
	*/

	//printf(" Verification 5 \n");

	vbx_dma_to_vector( vB,  coeff, MATRIX_SIZE * sizeof(int32_t));
	for(i = 0;i<MATRIX_SIZE; i++)
	{				
		vbx_dma_to_vector( vA,  TwoD_T[i], MATRIX_SIZE * sizeof(int32_t) );	
	/*	for (k=0;k<10;k++)
		{
		printf("%d   %d  \n", vA[k],TwoD_T[i][k]);
		}
		*/
	//	vbx_sync();
	//	printf(" Verification 6 \n");	
		vbx_acc( VVWS, VMULFXP, vD, vB, vA );
		vbx_dma_to_host( output, vD, sizeof(int32_t) );
		vbx_sync();
	//	printf(" Verification 7 \n");
		coeff[i] = *output;
	}
	//---------------Saving denoised vector to file-------------
	stop_time = timestamp();
	printf("\nTotal Runtime = %ld\n", stop_time-start_time);
	f = fopen("denoised_sim.txt", "w");
	if (f == NULL)
	{
	    printf("Error opening file!\n");
	    exit(1);
	}
	for (k=0;k<MATRIX_SIZE;k++)
	{
	fprintf(f, "%d\n", coeff[k]);
	}
	fclose(f);
	//printf(" Verification 8 \n");
	//---------------------------------------------------------------
	vbx_sp_free();
	vbx_shared_free(col1);
	vbx_shared_free(TwoD);
	vbx_shared_free(TwoD_T);
	vbx_shared_free(output);
	vbx_shared_free(coeff);
	
	//for (int k=0;k<20;k++)
	//{
	//	printf("The result - %d  \n", coeff[0]);
		//printf("%d   %d\n", d[0], sizeof(d));
	//}
	fclose(pinput);fclose(TM);fclose(TM_T);
	return 0;
}
