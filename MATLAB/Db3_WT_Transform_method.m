clc;
clear;
%-----------------------------------------------------------------------
% Transform based method for Db3 wavelet
% Minimum size of transformation matrix requires is 64-by-64 for level 4
% decomposition and 2048-by-2048 for level 9 decomposition
% In this code we calculate for level 4 decomposition for compaison with
% TCAS 2013 paper
% Madishetty, S.K.; Madanayake, A.; Cintra, R.J.; Dimitrov, V.S.; Mugler, D.H., 
% "VLSI Architectures for the 4-Tap and 6-Tap 2-D Daubechies Wavelet Filters Using 
% Algebraic Integers," Circuits and Systems I: Regular Papers, IEEE Transactions on ,
% vol.60, no.6, pp.1455,1468, June 2013
%-----------------------------------------------------------------------
dwtmode('per');
addpath('F:\Matlab_MXP_Simulations\Code')
addpath('F:\Matlab_MXP_Simulations\Code\Twos')
% addpath('/media/sidharth/UUI/Matlab_MXP_Simulations/Code')
cd('F:\Matlab_MXP_Simulations\Data\patient001'); 
% cd('/media/sidharth/UUI/Matlab_MXP_Simulations/Data/patient001');
li = dir;
names = li(3).name;
S = load(names,'-mat');
S=S.val; %S = int32(S);
[m,n] = size(S);
[Lo_D,Hi_D,Lo_R,Hi_R] = wfilters('db3');

Y = fliplr(Lo_D)'; Y = padarray(Y, 58, 'post');  Y = circshift(Y,-2);   
Z = fliplr(Hi_D)'; Z = padarray(Z, 58, 'post');  Z = circshift(Z,-2);   
for i = 1:2:63     
    A1(i,:) = Y';
    A1(i+1,:) = Z';
    Z = circshift(Z,2);
    Y = circshift(Y,2);
end

%---------------------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------
Y = fliplr(Lo_D)'; Y = padarray(Y, 26, 'post');  Y = circshift(Y,-2);
Z = fliplr(Hi_D)'; Z = padarray(Z, 26, 'post');  Z = circshift(Z,-2);
for i = 1:2:31     
    A2(i,:) = Y';
    A2(i+1,:) = Z';
    Z = circshift(Z,2);
    Y = circshift(Y,2);
end
TEMP = eye(64,64);
for j = 1:2:63
    TEMP(j,:) = zeros(64,1);
end
for k = 1:2:63
    for j = 1:1:32
        TEMP(k,(2*j)-1) = A2((k+1)/2,j);
    end
end
A2 = TEMP;
clear TEMP i j k;
%---------------------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------
Y = fliplr(Lo_D)'; Y = padarray(Y, 10, 'post');  Y = circshift(Y,-2);
Z = fliplr(Hi_D)'; Z = padarray(Z, 10, 'post');  Z = circshift(Z,-2);
for i = 1:2:15     
    A3(i,:) = Y';
    A3(i+1,:) = Z';
    Z = circshift(Z,2);
    Y = circshift(Y,2);
end
TEMP = eye(64,64);
for j = 1:4:63
    TEMP(j,:) = zeros(64,1);
end
for k = 1:4:63
    for j = 1:1:16
        TEMP(k,(4*j)-3) = A3((k+3)/4,j);
    end
end
A3 = TEMP;
clear TEMP i j k;
%---------------------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------
Y = fliplr(Lo_D)'; Y = padarray(Y, 2, 'post');  Y = circshift(Y,-2);
Z = fliplr(Hi_D)'; Z = padarray(Z, 2, 'post');  Z = circshift(Z,-2);
for i = 1:2:7     
    A4(i,:) = Y';
    A4(i+1,:) = Z';
    Z = circshift(Z,2);
    Y = circshift(Y,2);
end
TEMP = eye(64,64);
for j = 1:8:63
    TEMP(j,:) = zeros(64,1);
end
for k = 1:8:63
    for j = 1:1:8
        TEMP(k,(8*j)-7) = A4((k+7)/8,j);
    end
end
A4 = TEMP;
clear TEMP i j k;


% coeff_matrix1 = A1*X;
% coeff_matrix2 = A2*coeff_matrix1;
% coeff_matrix3 = A3*coeff_matrix2;
% coeff_matrix4 = A4*coeff_matrix3;

TM = (A4*(A3*(A2*A1)));
N = 32768;
tStart = tic; 
for i = 1:64:N
    X = S(1,i:i+63)';%X = X';
    coeff_matrix9_TM = TM*X;
    % TEMP = coeff_matrix9_TM;
    for j = 1:16:64
        coeff_matrix9_TM(j,1) = 0;
    end
    BW_Rem_X_matrix(i:i+63,1) = TM'*coeff_matrix9_TM;
end
tElapsed_1 = toc(tStart);
clear tStart;
%X = S(1,1:1024)';
tStart = tic; 
% [C,L] = wavedec(X,4,Lo_D,Hi_D);
% Temp_X = wrcoef('a',C,L,Lo_R,Hi_R,4);
% BW_Rem_X_DWT = X - Temp_X;
for i = 1:64:N
    X = S(1,i:i+63)';
    [C,L] = wavedec(X,4,Lo_D,Hi_D);
    Temp_X = wrcoef('a',C,L,Lo_R,Hi_R,4);
    BW_Rem_X_DWT(i:i+63,1) = X - Temp_X; 
end
tElapsed_2 = toc(tStart);
hold on;plot(BW_Rem_X_DWT,'r');plot(BW_Rem_X_matrix);hold off;
tElapsed_1,tElapsed_2, corrcoef(BW_Rem_X_matrix,BW_Rem_X_DWT)
q = quantizer([32,17]);
tm = num2int(q,TM);
%tm_mat = double(tm)-48;
%tm_mat = bi2de(tm_mat,'left-msb');
tm = reshape(tm,64,64);
S_FXD = num2int(q,S(1,1:N)); S_FXD = S_FXD';
%coeff_matrix9_TM_FXD = num2int(q,coeff_matrix9_TM);
BW_Rem_X_DWT_FXD = num2int(q, BW_Rem_X_DWT);


% asd = dec2twos(-11282330, 32);
% bin2num(q,asd);
% sim_results = dlmread('denoised_sim.txt');
% asd = dec2twos(sim_results, 32);
% asd = bin2num(q,asd);





