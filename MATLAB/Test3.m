 clc;clear;
%-----------------------------------------------------------------------
% SAME AS TEST1.M BUT WITH SYMLET 4 
%-----------------------------------------------------------------------
addpath('F:\Matlab_MXP_Simulations\Code')
% addpath('/media/sidharth/UUI/Matlab_MXP_Simulations/Code')
cd('F:\Matlab_MXP_Simulations\Data\patient001'); 
% cd('/media/sidharth/UUI/Matlab_MXP_Simulations/Data/patient001');
li = dir;
names = li(3).name;
S = load(names,'-mat');
S=S.val; %S = int32(S);
[m,n] = size(S);
[Lo_D,Hi_D,Lo_R,Hi_R] = wfilters('sym4');
% Lo_D = int32(Lo_D*(100000));
% Lo_R = int32(Lo_R*(100000));
% pow_of_2 = floor(log2(n));
% Dyad_len_of_sig = 2^pow_of_2;
% new_Dyad_len = Dyad_len_of_sig;
%Construction of DWT matrix for sym8
%Y = Lo_D'; Z = Hi_D';
Y = fliplr(Lo_D)'; Y = padarray(Y, 2040, 'post');  Y = circshift(Y,-2);   
Z = fliplr(Hi_D)'; Z = padarray(Z, 2040, 'post');  Z = circshift(Z,-4);   
A1(1,:) = Z';
for i = 2:2:3 
    Z = circshift(Z,2);
    A1(i,:) = Y';
    A1(i+1,:) = Z';
    Y = circshift(Y,2);
end
for j = 4:2:2047  
    Z = circshift(Z,2);
    A1(j,:) = Y';
    A1(j+1,:) = Z';
    Y = circshift(Y,2);    
end
% Z = circshift(Z,2);
A1(2048,:) = Y';
dwtmode('per');
X = S(1,1001:3048);X = X';
[C,L] = wavedec(X,9,Lo_D,Hi_D);
coeff_matrix1 = A1*X;
cA1_matrix1 = coeff_matrix1;
for q = 1:2:2048
    cA1_matrix1(q) = 0;   % cA_matrix with detailed coefficients all zeros
end
cA1_matrix2 = cA1_matrix1(cA1_matrix1 ~= 0);  % cA_matrix without zeros
% f = MakeONFilter('Symmlet',8);
% wc = FWT_PO(X,0,f);


% %------------------------------------------------------------------
% %------------------------------------------------------------------
% recon_X = A1' * cA_matrix1;
% plot(X,'r');hold on; plot(recon_X);hold off;
% %------------------------------------------------------------------
% reco_dwt_X = idwt(cA,[],Lo_R,Hi_R);
% recon_X = A1' * coeff_matrix;
% hold on;plot(X);plot(recon_X,'r');hold off;


%%%%%%%--------------------------------------------------------------------------------------------------------
%    2nd level
%%%%%%%--------------------------------------------------------------------------------------------------------
Y = fliplr(Lo_D)'; Y = padarray(Y, 1016, 'post');  Y = circshift(Y,-2);
Z = fliplr(Hi_D)'; Z = padarray(Z, 1016, 'post');  Z = circshift(Z,-4);
A2(1,:) = Z';
for i = 2:2:3   
    Z = circshift(Z,2);
    A2(i,:) = Y';
    A2(i+1,:) = Z';
    Y = circshift(Y,2);
end
for j = 4:2:1023     
    Z = circshift(Z,2);
    A2(j,:) = Y';
    A2(j+1,:) = Z';
    Y = circshift(Y,2);
        
end
A2(1024,:) = Y';
coeff_matrix2 = A2*cA1_matrix2;
cA2_matrix1 = coeff_matrix2;
for q = 1:2:1024
    cA2_matrix1(q) = 0;   % cA_matrix with detailed coefficients all zeros
end
cA2_matrix2 = cA2_matrix1(cA2_matrix1 ~= 0); 

%%%%%%%--------------------------------------------------------------------------------------------------------
%    3rd level
%%%%%%%--------------------------------------------------------------------------------------------------------
Y = fliplr(Lo_D)'; Y = padarray(Y, 504, 'post');  Y = circshift(Y,-2);
Z = fliplr(Hi_D)'; Z = padarray(Z, 504, 'post');  Z = circshift(Z,-4);
A3(1,:) = Z';
for i = 2:2:3   
    Z = circshift(Z,2);
    A3(i,:) = Y';
    A3(i+1,:) = Z';
    Y = circshift(Y,2);
end
for j = 4:2:511 
    Z = circshift(Z,2);
    A3(j,:) = Y';
    A3(j+1,:) = Z';    
    Y = circshift(Y,2);
end
A3(512,:) = Y';
coeff_matrix3 = A3*cA2_matrix2;
cA3_matrix1 = coeff_matrix3;
for q = 1:2:512
    cA3_matrix1(q) = 0;   % cA_matrix with detailed coefficients all zeros
end
cA3_matrix2 = cA3_matrix1(cA3_matrix1 ~= 0); 

%%%%%%%--------------------------------------------------------------------------------------------------------
%    4th level
%%%%%%%--------------------------------------------------------------------------------------------------------
Y = fliplr(Lo_D)'; Y = padarray(Y, 248, 'post');  Y = circshift(Y,-2);
Z = fliplr(Hi_D)'; Z = padarray(Z, 248, 'post');  Z = circshift(Z,-4);
A4(1,:) = Z';
for i = 2:2:3   
    Z = circshift(Z,2);
    A4(i,:) = Y';
    A4(i+1,:) = Z';
    Y = circshift(Y,2);
end
for j = 4:2:255 
    
    Z = circshift(Z,2);
    A4(j,:) = Y';
    A4(j+1,:) = Z';
    Y = circshift(Y,2);    
end
A4(256,:) = Y';
coeff_matrix4 = A4*cA3_matrix2;
cA4_matrix1 = coeff_matrix4;
for q = 1:2:256
    cA4_matrix1(q) = 0;   % cA_matrix with detailed coefficients all zeros
end
cA4_matrix2 = cA4_matrix1(cA4_matrix1 ~= 0); 

%%%%%%%--------------------------------------------------------------------------------------------------------
%    5th level
%%%%%%%--------------------------------------------------------------------------------------------------------
Y = fliplr(Lo_D)'; Y = padarray(Y, 120, 'post');  Y = circshift(Y,-2);
Z = fliplr(Hi_D)'; Z = padarray(Z, 120, 'post');  Z = circshift(Z,-4);
A5(1,:) = Z';
for i = 2:2:3   
    Z = circshift(Z,2);
    A5(i,:) = Y';
    A5(i+1,:) = Z';
    Y = circshift(Y,2);
end
for j = 4:2:127
    
    Z = circshift(Z,2);
    A5(j,:) = Y';
    A5(j+1,:) = Z';
    Y = circshift(Y,2);    
end
A5(128,:) = Y';
coeff_matrix5 = A5*cA4_matrix2;
cA5_matrix1 = coeff_matrix5;
for q = 1:2:128
    cA5_matrix1(q) = 0;   % cA_matrix with detailed coefficients all zeros
end
cA5_matrix2 = cA5_matrix1(cA5_matrix1 ~= 0); 

%%%%%%%--------------------------------------------------------------------------------------------------------
%    6th level
%%%%%%%--------------------------------------------------------------------------------------------------------
Y = fliplr(Lo_D)'; Y = padarray(Y, 56, 'post');  Y = circshift(Y,-2); 
Z = fliplr(Hi_D)'; Z = padarray(Z, 56, 'post');  Z = circshift(Z,-4);  
A6(1,:) = Z';
for i = 2:2:3   
    Z = circshift(Z,2);
    A6(i,:) = Y';
    A6(i+1,:) = Z';
    Y = circshift(Y,2);
end
for j = 4:2:63
    
    Z = circshift(Z,2);
    A6(j,:) = Y';
    A6(j+1,:) = Z';
    Y = circshift(Y,2);    
end
A6(64,:) = Y';
coeff_matrix6 = A6*cA5_matrix2;
cA6_matrix1 = coeff_matrix6;
for q = 1:2:64
    cA6_matrix1(q) = 0;   % cA_matrix with detailed coefficients all zeros
end
cA6_matrix2 = cA6_matrix1(cA6_matrix1 ~= 0); 


%%%%%%%--------------------------------------------------------------------------------------------------------
%    7th level
%%%%%%%--------------------------------------------------------------------------------------------------------
Y = fliplr(Lo_D)'; Y = padarray(Y, 24, 'post');  Y = circshift(Y,-2); 
Z = fliplr(Hi_D)'; Z = padarray(Z, 24, 'post');  Z = circshift(Z,-4);  
A7(1,:) = Z';
for i = 2:2:3   
    Z = circshift(Z,2);
    A7(i,:) = Y';
    A7(i+1,:) = Z';
    Y = circshift(Y,2);
end
for j = 4:2:31
    
    Z = circshift(Z,2);
    A7(j,:) = Y';
    A7(j+1,:) = Z';
    Y = circshift(Y,2);    
end
A7(32,:) = Y';
coeff_matrix7 = A7*cA6_matrix2;
cA7_matrix1 = coeff_matrix7;
for q = 1:2:32
    cA7_matrix1(q) = 0;   % cA_matrix with detailed coefficients all zeros
end
cA7_matrix2 = cA7_matrix1(cA7_matrix1 ~= 0); 

%%%%%%%--------------------------------------------------------------------------------------------------------
%    8th level
%%%%%%%--------------------------------------------------------------------------------------------------------
Y = fliplr(Lo_D)'; Y = padarray(Y, 8, 'post');  Y = circshift(Y,-2); 
Z = fliplr(Hi_D)'; Z = padarray(Z, 8, 'post');  Z = circshift(Z,-4);
A8(1,:) = Z';
for i = 2:2:3   
    Z = circshift(Z,2);
    A8(i,:) = Y';
    A8(i+1,:) = Z';
    Y = circshift(Y,2);
end
for j = 4:2:15
    
    Z = circshift(Z,2);
    A8(j,:) = Y';
    A8(j+1,:) = Z';
    Y = circshift(Y,2);    
end
A8(16,:) = Y';
coeff_matrix8 = A8*cA7_matrix2;
cA8_matrix1 = coeff_matrix8;
for q = 1:2:16
    cA8_matrix1(q) = 0;   % cA_matrix with detailed coefficients all zeros
end
cA8_matrix2 = cA8_matrix1(cA8_matrix1 ~= 0); 

%%%%%%%--------------------------------------------------------------------------------------------------------
%    9 th level
%%%%%%%--------------------------------------------------------------------------------------------------------
Y = fliplr(Lo_D)';   Y = circshift(Y,-2); %Y = padarray(Y, 8, 'post');
Z = fliplr(Hi_D)';   Z = circshift(Z,-4); %Z = padarray(Z, 8, 'post');
A9(1,:) = Z';
for i = 2:2:3   
    Z = circshift(Z,2);
    A9(i,:) = Y';
    A9(i+1,:) = Z';
    Y = circshift(Y,2);
end
for j = 4:2:7
    
    Z = circshift(Z,2);
    A9(j,:) = Y';
    A9(j+1,:) = Z';
    Y = circshift(Y,2);    
end
A9(8,:) = Y';
coeff_matrix9 = A9*cA8_matrix2;
cA9_matrix1 = coeff_matrix9;
for q = 1:2:8
    cA9_matrix1(q) = 0;   % cA_matrix with detailed coefficients all zeros
end
cA9_matrix2 = cA9_matrix1(cA9_matrix1 ~= 0);


%%--------------- FFT analysis --------------------------------------
% cA_fft = fft(C(1:4));
% cA_matrix2_fft = fft(cA9_matrix2);
% N = 4;
% fvec = (0:1/N:0.5-1/N)';
% pgram1 = abs(cA_fft(1:length(fvec))).^2/N;
% pgram2 = abs(cA_matrix2_fft(1:length(fvec))).^2/N;
% hold on;stem(fvec,pgram1,'b');stem(fvec,pgram2,'r');hold off;

%%------------RECONSTRUCTION OF 9th LEVEL COEFFICIENTS--------------------

Rec_cA8 =  zeros(16,1);
B = A9'*cA9_matrix1;
for i = 1:1:8
    Rec_cA8(2*i,1) = B(i);
end
clear B i;
%%------------------------------------------
Rec_cA7 =  zeros(32,1);
B = A8'*Rec_cA8;
for i = 1:1:16
    Rec_cA7(2*i,1) = B(i);
end
clear B i;
%%------------------------------------------
Rec_cA6 =  zeros(64,1);
B = A7'*Rec_cA7;
for i = 1:1:32
    Rec_cA6(2*i,1) = B(i);
end
clear B i;
%%------------------------------------------
Rec_cA5 =  zeros(128,1);
B = A6'*Rec_cA6;
for i = 1:1:64
    Rec_cA5(2*i,1) = B(i);
end
clear B i;
%%------------------------------------------
Rec_cA4 =  zeros(256,1);
B = A5'*Rec_cA5;
for i = 1:1:128
    Rec_cA4(2*i,1) = B(i);
end
clear B i;
%%------------------------------------------
Rec_cA3 =  zeros(512,1);
B = A4'*Rec_cA4;
for i = 1:1:256
    Rec_cA3(2*i,1) = B(i);
end
clear B i;
%%------------------------------------------
Rec_cA2 =  zeros(1024,1);
B = A3'*Rec_cA3;
for i = 1:1:512
    Rec_cA2(2*i,1) = B(i);
end
clear B i;
%%------------------------------------------
Rec_cA1 =  zeros(2048,1);
B = A2'*Rec_cA2;
for i = 1:1:1024
    Rec_cA1(2*i,1) = B(i);
end
Temp_X_matrix = A1'*Rec_cA1;

BW_Rem_X_matrix = X - Temp_X_matrix;
Temp_X = wrcoef('a',C,L,Lo_R,Hi_R,9);

BW_Rem_X_DWT = X - Temp_X;
hold on;plot(BW_Rem_X_matrix);plot(BW_Rem_X_DWT,'r');hold off;



