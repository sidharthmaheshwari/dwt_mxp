clc;
clear;
%-----------------------------------------------------------------------
% SAME AS TEST1.M BUT WITH SYMLET 4 
%-----------------------------------------------------------------------
addpath('F:\Matlab_MXP_Simulations\Code')
addpath('F:\Matlab_MXP_Simulations\Code\Twos')
% addpath('/media/sidharth/UUI/Matlab_MXP_Simulations/Code')
cd('F:\Matlab_MXP_Simulations\Data\patient001'); 
% cd('/media/sidharth/UUI/Matlab_MXP_Simulations/Data/patient001');
li = dir;
names = li(3).name;
S = load(names,'-mat');
S=S.val; %S = int32(S);
[m,n] = size(S);
[Lo_D,Hi_D,Lo_R,Hi_R] = wfilters('sym4');
% Lo_D = int32(Lo_D*(100000));
% Lo_R = int32(Lo_R*(100000));
% pow_of_2 = floor(log2(n));
% Dyad_len_of_sig = 2^pow_of_2;
% new_Dyad_len = Dyad_len_of_sig;
%Construction of DWT matrix for sym8
%Y = Lo_D'; Z = Hi_D';
Y = fliplr(Lo_D)'; Y = padarray(Y, 2040, 'post');  Y = circshift(Y,-2);   
Z = fliplr(Hi_D)'; Z = padarray(Z, 2040, 'post');  Z = circshift(Z,-4);   
A1(1,:) = Z';
for i = 2:2:3 
    Z = circshift(Z,2);
    A1(i,:) = Y';
    A1(i+1,:) = Z';
    Y = circshift(Y,2);
end
for j = 4:2:2047  
    Z = circshift(Z,2);
    A1(j,:) = Y';
    A1(j+1,:) = Z';
    Y = circshift(Y,2);    
end
% Z = circshift(Z,2);
A1(2048,:) = Y';
dwtmode('per');
X = S(1,1:2048);X = X';
[C,L] = wavedec(X,9,Lo_D,Hi_D);
coeff_matrix1 = A1*X;
%---------------------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------
Y = fliplr(Lo_D)'; Y = padarray(Y, 1016, 'post');  Y = circshift(Y,-2);
Z = fliplr(Hi_D)'; Z = padarray(Z, 1016, 'post');  Z = circshift(Z,-4);
A2(1,:) = Z';
for i = 2:2:3   
    Z = circshift(Z,2);
    A2(i,:) = Y';
    A2(i+1,:) = Z';
    Y = circshift(Y,2);
end
for j = 4:2:1023     
    Z = circshift(Z,2);
    A2(j,:) = Y';
    A2(j+1,:) = Z';
    Y = circshift(Y,2);
        
end
A2(1024,:) = Y';

TEMP = eye(2048,2048);
for j = 2:2:2048
    TEMP(j,:) = zeros(2048,1);
end
for k = 2:2:2048
    for j = 1:1:1024
        TEMP(k,2*j) = A2(k/2,j);
    end
end
A2 = TEMP;
coeff_matrix2 = A2*coeff_matrix1;
clear TEMP i j k;
%---------------------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------
Y = fliplr(Lo_D)'; Y = padarray(Y, 504, 'post');  Y = circshift(Y,-2);
Z = fliplr(Hi_D)'; Z = padarray(Z, 504, 'post');  Z = circshift(Z,-4);
A3(1,:) = Z';
for i = 2:2:3   
    Z = circshift(Z,2);
    A3(i,:) = Y';
    A3(i+1,:) = Z';
    Y = circshift(Y,2);
end
for j = 4:2:511 
    Z = circshift(Z,2);
    A3(j,:) = Y';
    A3(j+1,:) = Z';    
    Y = circshift(Y,2);
end
A3(512,:) = Y';

TEMP = eye(2048,2048);
for j = 4:4:2048
    TEMP(j,:) = zeros(2048,1);
end
for k = 4:4:2048
    for j = 1:1:512
        TEMP(k,4*j) = A3(k/4,j);
    end
end
A3 = TEMP;
coeff_matrix3 = A3*coeff_matrix2;
clear TEMP i j k;
%---------------------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------
Y = fliplr(Lo_D)'; Y = padarray(Y, 248, 'post');  Y = circshift(Y,-2);
Z = fliplr(Hi_D)'; Z = padarray(Z, 248, 'post');  Z = circshift(Z,-4);
A4(1,:) = Z';
for i = 2:2:3   
    Z = circshift(Z,2);
    A4(i,:) = Y';
    A4(i+1,:) = Z';
    Y = circshift(Y,2);
end
for j = 4:2:255 
    
    Z = circshift(Z,2);
    A4(j,:) = Y';
    A4(j+1,:) = Z';
    Y = circshift(Y,2);    
end
A4(256,:) = Y';
TEMP = eye(2048,2048);
for j = 8:8:2048
    TEMP(j,:) = zeros(2048,1);
end
for k = 8:8:2048
    for j = 1:1:256
        TEMP(k,8*j) = A4(k/8,j);
    end
end
A4 = TEMP;
coeff_matrix4 = A4*coeff_matrix3;
clear TEMP i j k;

%---------------------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------
Y = fliplr(Lo_D)'; Y = padarray(Y, 120, 'post');  Y = circshift(Y,-2);
Z = fliplr(Hi_D)'; Z = padarray(Z, 120, 'post');  Z = circshift(Z,-4);
A5(1,:) = Z';
for i = 2:2:3   
    Z = circshift(Z,2);
    A5(i,:) = Y';
    A5(i+1,:) = Z';
    Y = circshift(Y,2);
end
for j = 4:2:127
    
    Z = circshift(Z,2);
    A5(j,:) = Y';
    A5(j+1,:) = Z';
    Y = circshift(Y,2);    
end
A5(128,:) = Y';
TEMP = eye(2048,2048);
for j = 16:16:2048
    TEMP(j,:) = zeros(2048,1);
end
for k = 16:16:2048
    for j = 1:1:128
        TEMP(k,16*j) = A5(k/16,j);
    end
end
A5 = TEMP;
coeff_matrix5 = A5*coeff_matrix4;
clear TEMP i j k;

%---------------------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------
Y = fliplr(Lo_D)'; Y = padarray(Y, 56, 'post');  Y = circshift(Y,-2); 
Z = fliplr(Hi_D)'; Z = padarray(Z, 56, 'post');  Z = circshift(Z,-4);  
A6(1,:) = Z';
for i = 2:2:3   
    Z = circshift(Z,2);
    A6(i,:) = Y';
    A6(i+1,:) = Z';
    Y = circshift(Y,2);
end
for j = 4:2:63
    
    Z = circshift(Z,2);
    A6(j,:) = Y';
    A6(j+1,:) = Z';
    Y = circshift(Y,2);    
end
A6(64,:) = Y';
TEMP = eye(2048,2048);
for j = 32:32:2048
    TEMP(j,:) = zeros(2048,1);
end
for k = 32:32:2048
    for j = 1:1:64
        TEMP(k,32*j) = A6(k/32,j);
    end
end
A6 = TEMP;
coeff_matrix6 = A6*coeff_matrix5;
clear TEMP i j k;
%---------------------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------
Y = fliplr(Lo_D)'; Y = padarray(Y, 24, 'post');  Y = circshift(Y,-2); 
Z = fliplr(Hi_D)'; Z = padarray(Z, 24, 'post');  Z = circshift(Z,-4);  
A7(1,:) = Z';
for i = 2:2:3   
    Z = circshift(Z,2);
    A7(i,:) = Y';
    A7(i+1,:) = Z';
    Y = circshift(Y,2);
end
for j = 4:2:31
    
    Z = circshift(Z,2);
    A7(j,:) = Y';
    A7(j+1,:) = Z';
    Y = circshift(Y,2);    
end
A7(32,:) = Y';
TEMP = eye(2048,2048);
for j = 64:64:2048
    TEMP(j,:) = zeros(2048,1);
end
for k = 64:64:2048
    for j = 1:1:32
        TEMP(k,64*j) = A7(k/64,j);
    end
end
A7 = TEMP;
coeff_matrix7 = A7*coeff_matrix6;
clear TEMP i j k;

%%%%%%%--------------------------------------------------------------------------------------------------------
%    8th level
%%%%%%%--------------------------------------------------------------------------------------------------------
Y = fliplr(Lo_D)'; Y = padarray(Y, 8, 'post');  Y = circshift(Y,-2); 
Z = fliplr(Hi_D)'; Z = padarray(Z, 8, 'post');  Z = circshift(Z,-4);
A8(1,:) = Z';
for i = 2:2:3   
    Z = circshift(Z,2);
    A8(i,:) = Y';
    A8(i+1,:) = Z';
    Y = circshift(Y,2);
end
for j = 4:2:15
    
    Z = circshift(Z,2);
    A8(j,:) = Y';
    A8(j+1,:) = Z';
    Y = circshift(Y,2);    
end
A8(16,:) = Y';
TEMP = eye(2048,2048);
for j = 128:128:2048
    TEMP(j,:) = zeros(2048,1);
end
for k = 128:128:2048
    for j = 1:1:16
        TEMP(k,128*j) = A8(k/128,j);
    end
end
A8 = TEMP;
coeff_matrix8 = A8*coeff_matrix7;
clear TEMP i j k;
%%%%%%%--------------------------------------------------------------------------------------------------------
%    9 th level
%%%%%%%--------------------------------------------------------------------------------------------------------
Y = fliplr(Lo_D)';   Y = circshift(Y,-2); %Y = padarray(Y, 8, 'post');
Z = fliplr(Hi_D)';   Z = circshift(Z,-4); %Z = padarray(Z, 8, 'post');
A9(1,:) = Z';
for i = 2:2:3   
    Z = circshift(Z,2);
    A9(i,:) = Y';
    A9(i+1,:) = Z';
    Y = circshift(Y,2);
end
for j = 4:2:7
    
    Z = circshift(Z,2);
    A9(j,:) = Y';
    A9(j+1,:) = Z';
    Y = circshift(Y,2);    
end
A9(8,:) = Y';
TEMP = eye(2048,2048);
for j = 256:256:2048
    TEMP(j,:) = zeros(2048,1);
end
for k = 256:256:2048
    for j = 1:1:8
        TEMP(k,256*j) = A9(k/256,j);
    end
end
A9 = TEMP;
coeff_matrix9 = A9*coeff_matrix8;
clear TEMP i j k;

TM = A9*(A8*(A7*(A6*(A5*(A4*(A3*(A2*A1)))))));
tStart = tic; 
coeff_matrix9_TM = TM*X;
TEMP = coeff_matrix9_TM;
for j = 512:512:2048
    TEMP(j,1) = 0;
end
BW_Rem_X_matrix = TM'*TEMP;
tElapsed_1 = toc(tStart);
clear tStart;
tStart = tic; 
Temp_X = wrcoef('a',C,L,Lo_R,Hi_R,9);
BW_Rem_X_DWT = X - Temp_X;
tElapsed_2 = toc(tStart);
% hold on;plot(BW_Rem_X_matrix);plot(BW_Rem_X_DWT,'r');hold off;
tElapsed_1,tElapsed_2
q = quantizer([32,17]);
tm = num2int(q,TM);
%tm_mat = double(tm)-48;
%tm_mat = bi2de(tm_mat,'left-msb');
tm = reshape(tm,2048,2048);
S_FXD = num2int(q,X); S_FXD = S_FXD';
coeff_matrix9_TM_FXD = num2int(q,coeff_matrix9_TM);

% 
% asd = dec2twos(-11282330, 32);
% bin2num(q,asd);
% sim_results = dlmread('denoised_sim.txt');
% asd = dec2twos(sim_results, 32);
% asd = bin2num(q,asd);





