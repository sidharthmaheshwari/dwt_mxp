clc;clear;
 addpath('F:\Matlab_MXP_Simulations\Code')
cd('F:\Matlab_MXP_Simulations\Data\patient001'); 
li = dir;
names = li(3).name;
S = load(names,'-mat');
S=S.val; %S = int32(S);
[m,n] = size(S);
[Lo_D,Hi_D,~,~] = wfilters('sym8');
% Lo_D = int32(Lo_D*(100000));
% Lo_R = int32(Lo_R*(100000));
% pow_of_2 = floor(log2(n));
% Dyad_len_of_sig = 2^pow_of_2;
% new_Dyad_len = Dyad_len_of_sig;
%Construction of DWT matrix for sym8
%Y = Lo_D'; Z = Hi_D';
Y = fliplr(Lo_D)';
Z = fliplr(Hi_D)';
Y = padarray(Y, 496, 'post');
Z = padarray(Z, 496, 'post');
[LBM, RBM] = boundary(Lo_D);

LBM = padarray(LBM', 498, 'post');

RBM = padarray(RBM', 498, 'post');

A(1:7,:) = LBM';
for j = 8:2:504      
    A(j,:) = Y';
    A(j+1,:) = Z';
    Z = circshift(Z,2);
    Y = circshift(Y,2);    
end
A(506:512,:) = RBM';
dwtmode('per');
X = S(1,1:512);X = X';
[cA,cD] = dwt(X,Lo_D,Hi_D);

cA_matrix2 = A*X;