 clc;clear;
 addpath('F:\Matlab_MXP_Simulations\Code')
cd('F:\Matlab_MXP_Simulations\Data\patient001'); 
li = dir;
names = li(3).name;
S = load(names,'-mat');
S=S.val; %S = int32(S);
[m,n] = size(S);
[Lo_D,Hi_D,Lo_R,Hi_R] = wfilters('sym4');
% Lo_D = int32(Lo_D*(100000));
% Lo_R = int32(Lo_R*(100000));
% pow_of_2 = floor(log2(n));
% Dyad_len_of_sig = 2^pow_of_2;
% new_Dyad_len = Dyad_len_of_sig;
%Construction of DWT matrix for sym8
%Y = Lo_D'; Z = Hi_D';
Y = fliplr(Lo_D)'; Y = padarray(Y, 496, 'post');  Y = circshift(Y,-6);   % For symlet 8 Y = circshift(Y,-6);
Z = fliplr(Hi_D)'; Z = padarray(Z, 496, 'post');  Z = circshift(Z,-8);   % For symlet 8 Z = circshift(Z,-8); 
A1(1,:) = Z';
for i = 2:2:8   
    Z = circshift(Z,2);
    A1(i,:) = Y';
    A1(i+1,:) = Z';
    Y = circshift(Y,2);
end
for j = 10:2:511  
    Z = circshift(Z,2);
    A1(j,:) = Y';
    A1(j+1,:) = Z';
    Y = circshift(Y,2);    
end
% Z = circshift(Z,2);
A1(512,:) = Y';
dwtmode('per');
X = S(1,1001:1512);X = X';
[C,L] = wavedec(X,9,Lo_D,Hi_D);
coeff_matrix1 = A1*X;
cA1_matrix1 = coeff_matrix1;
for q = 1:2:512
    cA1_matrix1(q) = 0;   % cA_matrix with detailed coefficients all zeros
end
cA1_matrix2 = cA1_matrix1(cA1_matrix1 ~= 0);  % cA_matrix without zeros
% f = MakeONFilter('Symmlet',8);
% wc = FWT_PO(X,0,f);


% %------------------------------------------------------------------
% %------------------------------------------------------------------
% recon_X = A1' * cA_matrix1;
% plot(X,'r');hold on; plot(recon_X);hold off;
% %------------------------------------------------------------------
% reco_dwt_X = idwt(cA,[],Lo_R,Hi_R);
% recon_X = A1' * coeff_matrix;
% hold on;plot(X);plot(recon_X,'r');hold off;


%%%%%%%--------------------------------------------------------------------------------------------------------
%%%%%%%--------------------------------------------------------------------------------------------------------
Y = fliplr(Lo_D)'; Y = padarray(Y, 240, 'post');  Y = circshift(Y,-6);
Z = fliplr(Hi_D)'; Z = padarray(Z, 240, 'post');  Z = circshift(Z,-8);
A2(1,:) = Z';
for i = 2:2:8   
    Z = circshift(Z,2);
    A2(i,:) = Y';
    A2(i+1,:) = Z';
    Y = circshift(Y,2);
end
for j = 10:2:255 
    Y = circshift(Y,2);
    Z = circshift(Z,2);
    A2(j,:) = Y';
    A2(j+1,:) = Z';
        
end
A2(256,:) = Y';
coeff_matrix2 = A2*cA1_matrix2;
cA2_matrix1 = coeff_matrix2;
for q = 1:2:256
    cA2_matrix1(q) = 0;   % cA_matrix with detailed coefficients all zeros
end
cA2_matrix2 = cA2_matrix1(cA2_matrix1 ~= 0); 

%%%%%%%--------------------------------------------------------------------------------------------------------
%%%%%%%--------------------------------------------------------------------------------------------------------
Y = fliplr(Lo_D)'; Y = padarray(Y, 112, 'post');  Y = circshift(Y,-6);
Z = fliplr(Hi_D)'; Z = padarray(Z, 112, 'post');  Z = circshift(Z,-8);
A3(1,:) = Z';
for i = 2:2:8   
    Z = circshift(Z,2);
    A3(i,:) = Y';
    A3(i+1,:) = Z';
    Y = circshift(Y,2);
end
for j = 10:2:127 
    Y = circshift(Y,2);
    Z = circshift(Z,2);
    A3(j,:) = Y';
    A3(j+1,:) = Z';
        
end
A3(128,:) = Y';
coeff_matrix3 = A3*cA2_matrix2;
cA3_matrix1 = coeff_matrix3;
for q = 1:2:128
    cA3_matrix1(q) = 0;   % cA_matrix with detailed coefficients all zeros
end
cA3_matrix2 = cA3_matrix1(cA3_matrix1 ~= 0); 

%%%%%%%--------------------------------------------------------------------------------------------------------
%%%%%%%--------------------------------------------------------------------------------------------------------
Y = fliplr(Lo_D)'; Y = padarray(Y, 48, 'post');  Y = circshift(Y,-6);
Z = fliplr(Hi_D)'; Z = padarray(Z, 48, 'post');  Z = circshift(Z,-8);
A4(1,:) = Z';
for i = 2:2:8   
    Z = circshift(Z,2);
    A4(i,:) = Y';
    A4(i+1,:) = Z';
    Y = circshift(Y,2);
end
for j = 10:2:63 
    Y = circshift(Y,2);
    Z = circshift(Z,2);
    A4(j,:) = Y';
    A4(j+1,:) = Z';
        
end
A4(64,:) = Y';
coeff_matrix4 = A4*cA3_matrix2;
cA4_matrix1 = coeff_matrix4;
for q = 1:2:64
    cA4_matrix1(q) = 0;   % cA_matrix with detailed coefficients all zeros
end
cA4_matrix2 = cA4_matrix1(cA4_matrix1 ~= 0); 

%%%%%%%--------------------------------------------------------------------------------------------------------
%%%%%%%--------------------------------------------------------------------------------------------------------
Y = fliplr(Lo_D)'; Y = padarray(Y, 16, 'post');  Y = circshift(Y,-6);
Z = fliplr(Hi_D)'; Z = padarray(Z, 16, 'post');  Z = circshift(Z,-8);
A5(1,:) = Z';
for i = 2:2:8   
    Z = circshift(Z,2);
    A5(i,:) = Y';
    A5(i+1,:) = Z';
    Y = circshift(Y,2);
end
for j = 10:2:31
    Y = circshift(Y,2);
    Z = circshift(Z,2);
    A5(j,:) = Y';
    A5(j+1,:) = Z';
        
end
A5(32,:) = Y';
coeff_matrix5 = A5*cA4_matrix2;
cA5_matrix1 = coeff_matrix5;
for q = 1:2:32
    cA5_matrix1(q) = 0;   % cA_matrix with detailed coefficients all zeros
end
cA5_matrix2 = cA5_matrix1(cA5_matrix1 ~= 0); 

%%%%%%%--------------------------------------------------------------------------------------------------------
%%%%%%%--------------------------------------------------------------------------------------------------------
Y = fliplr(Lo_D)'; Y = circshift(Y,-6);%Y = padarray(Y, 16, 'post');  
Z = fliplr(Hi_D)'; Z = circshift(Z,-8);%Z = padarray(Z, 16, 'post');  
A6(1,:) = Z';
for i = 2:2:8   
    Z = circshift(Z,2);
    A6(i,:) = Y';
    A6(i+1,:) = Z';
    Y = circshift(Y,2);
end
for j = 10:2:15
    Y = circshift(Y,2);
    Z = circshift(Z,2);
    A6(j,:) = Y';
    A6(j+1,:) = Z';
        
end
A6(16,:) = Y';
coeff_matrix6 = A6*cA5_matrix2;
cA6_matrix1 = coeff_matrix6;
for q = 1:2:16
    cA6_matrix1(q) = 0;   % cA_matrix with detailed coefficients all zeros
end
cA6_matrix2 = cA6_matrix1(cA6_matrix1 ~= 0); 


%%%%%%%--------------------------------------------------------------------------------------------------------
%%%%%%%--------------------------------------------------------------------------------------------------------
A7 = A6;%(1:8,9:16);

coeff_matrix7 = A7* [cA6_matrix2;cA6_matrix2];
cA7_matrix1 = coeff_matrix7;
for q = 1:2:16
    cA7_matrix1(q) = 0;   % cA_matrix with detailed coefficients all zeros
end
cA7_matrix2 = cA7_matrix1(cA7_matrix1 ~= 0); 
cA7_matrix2 = cA7_matrix2(1:4);


%%%%%%%--------------------------------------------------------------------------------------------------------
%%%%%%%--------------------------------------------------------------------------------------------------------
A8 = A6;%A7(1:4,1:4);

coeff_matrix8 = A8*[cA7_matrix2;cA7_matrix2;cA7_matrix2;cA7_matrix2];
cA8_matrix1 = coeff_matrix8;
for q = 1:2:16
    cA8_matrix1(q) = 0;   % cA_matrix with detailed coefficients all zeros
end
cA8_matrix2 = cA8_matrix1(cA8_matrix1 ~= 0); 
cA8_matrix2 = cA8_matrix2(1:2);

%%%%%%%--------------------------------------------------------------------------------------------------------
%%%%%%%--------------------------------------------------------------------------------------------------------
A9 = A6;%A7(1:4,1:4);

coeff_matrix9 = A9*[cA8_matrix2;cA8_matrix2;cA8_matrix2;cA8_matrix2;cA8_matrix2;cA8_matrix2;cA8_matrix2;cA8_matrix2];
cA9_matrix1 = coeff_matrix9;
for q = 1:2:16
    cA9_matrix1(q) = 0;   % cA_matrix with detailed coefficients all zeros
end
cA9_matrix2 = cA9_matrix1(cA9_matrix1 ~= 0); 
cA9_matrix2 = cA9_matrix2(1);


%%--------------- FFT analysis --------------------------------------
% cA_fft = fft(cA);
% cA_matrix2_fft = fft(cA_matrix2);
% N = 256;
% fvec = (0:1/N:0.5-1/N)';
% pgram1 = abs(cA_fft(1:length(fvec))).^2/N;
% pgram2 = abs(cA_matrix2_fft(1:length(fvec))).^2/N;
% hold on;stem(fvec,pgram1,'b');stem(fvec,pgram2,'r');hold off;














