# README #



### What is this repository for? ###

* DWT based denoising of ECG and MXP implementation on Zedboard and Microzed

# FOLDER STRUCTURE #
##DWT_MXP##
#1. MATLAB - Matlab codes with a patients data



#2. MXP    - C codes for simulator and hardware implementation on Zedboard/microzed    
       1. Simulator_Case_1
       2. Hardware_sym4_2048

Simulator_Case_1 - Simulator working code for sym4 wavelet transform using 2048 samples from 1st lead of 1 patient of PTBDB database. It contains transfomation matrix "TM_FXD" and its transpose "TM_FXD_T", "denoised_sim" and "coeff9_sim" are the results obtained from simulator. All are in fixed point notation 16.16.

Hardware_Sym4_2048 - "sym4_9lvl.c" is hardware functional code for Zedboard using OS platform. It along with relevant input files i.e. TM_FXD, TM_FXD_T and patients data will also produce denoised samples as output. All in fixed point.